//Load express module with `require` directive
var express = require('express')
var app = express()
var saml = require('saml-encoder-decoder-js');
const bodyParser = require('body-parser');

app.use(bodyParser.text());


//Define request response in root URL (/)
app.get('/', function (req, res) {
  var xml = '<html>\n\
  <body>\n\
  Hello World\n\
  </body>\n\
  </html>'
  res.send(xml)

})



//Define request response in root URL (/)
app.post('/encode', function (req, res) {
  //var xml = '<?xml version="1.0" encoding="UTF-8"?><samlp:AuthnRequest     xmlns:samlp="urn:oasis:names:tc:SAML:2.0:protocol"    ID="dghihjdeegajbpejllikgcpnkceilmnmblhaidgl"     Version="2.0"    IssueInstant="2016-01-27T19:57:05Z"    ProtocolBinding="urn:oasis:names.tc:SAML:2.0:bindings:HTTP-Redirect"    ProviderName="pennmutual.com/prodplace"    AssertionConsumerServiceURL=""/>';
  var xml = req.body
  saml.encodeSamlRedirect(xml, function(err, encoded) {
    if (!err) {
      console.log("Redirect encoded string", encoded);
      res.send(encoded)
    }
  });


})

//Launch listening server on port 8081
app.listen(8081, function () {
  console.log('app listening on port 8081!')
})
